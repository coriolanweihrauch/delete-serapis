<?php
class SerapisCommand extends CConsoleCommand
{
	var $bVerbose;

	/* 	Standard upgrade to 256MB RAM */
	public function __construct()
	{
		$memory = "256M";
		if($this->bVerbose)
			echo "Trying to set the memory limit to ".$memory."\n";
		ini_set("memory_limit",$memory);
		ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT  & ~E_DEPRECATED);
	}

  public function getHelp()
  {
        return <<<EOD
USAGE
=====
yiic serapis <command> [<params>]

PARAMETERS
==========
asyncto             # Batch download data from ASyncTo
--bDownload         = true  - download records
--bImport           = true  - import downloaded records
--iCap              = null  - limit of number of records to download. 0=no limit
--bVerbose          = false - display verbose output

backup              # Backup database and delete old backups (more than 30 days)
--bVerbose          = false - display verbose output
--bCreate           = true  - create new backup
--bDelete           = true  - delete old backups (usually 30 days but depends on Lookup setting)

sendReminder        # Send reminders from mailqueue
--bReallySend       = false - actually send email
--bVerbose          = false - display verbose output

EOD;
  }

	public function actionIndex()
	{
		echo $this->getHelp();
	}

	/* Create backup and delete those older than 30 days */
	public function actionBackup($bVerbose=false, $bCreate=true, $bDelete=true)
	{
		if($bVerbose)
			$bVerbose == "true" ? $bVerbose = true : $bVerbose = false;
		$this->bVerbose = $bVerbose;
		if($bCreate)
			$bCreate == "true" ? $bCreate = true : $bCreate = false;
		if($bDelete)
			$bDelete == "true" ? $bDelete = true : $bDelete = false;

 		if($this->bVerbose)
			echo "\n".date("Y-m-d H:i:s")." - Processing Backup\n";

		// Create backup
		if($bCreate)
		{
			if($this->bVerbose)
				echo "\n== Creating backup ==\n";
	    $action = Yii::createComponent('application.modules.backup.controllers.DefaultController', $this, 'createShell');
	    $res = $action->actionCreateShell();
	    if($res)
	    {
    		if($this->bVerbose)
		    	echo "[OK] Created backup ".realpath($res)."\n";
	    }
	    else
	    	echo "ERROR[1] could not create backup !\n";
		}

	  // Remove old backups
	  if($bDelete)
	  {
   		if($this->bVerbose)
	 			echo "\n== Cleaning up backups ==\n";
			$backupParam = Yii::app()->params['backup'];
			$arrFiles = scandir(Yii::app()->basePath.$backupParam['directory']);
			$d = 0;
			$k = 0;
			foreach($arrFiles as $i => $f)
			{
				// is a file in the form $backupParam['filename']_date
				if(strlen($f) > strlen($backupParam['filename']) &&
					substr($f,0,strlen($backupParam['filename'])) == $backupParam['filename'])
				{
					// backup older than $backupParam['deleteafter'] days
					if(intval(substr($f,strlen($backupParam['filename']),8)) - (intval(date('Ymd', strtotime("-".$backupParam['deleteafter']." days")))) < 0)
					{
						if($this->bVerbose)
						{
							echo $f." - older than ".$backupParam['deleteafter']." days - Delete\n";
							echo "deleting ".Yii::app()->basePath.$backupParam['directory']."/".$f."\n";
						}
						// Delete file
						if(unlink(Yii::app()->basePath.$backupParam['directory']."/".$f))
						{
			    		if($this->bVerbose)
								echo " ... done\n";
							$d++;
						}
						else
						{
							echo $f." - error deleting\n\n";
						}
					}
					// Keep file
					else
					{
						if($this->bVerbose)
							echo $f." - keep\n";
						$k ++;
					}
				}
			}
   		if($this->bVerbose)
				echo "\nCompleted - Kept ".$k." files and deleted ".$d."\n";
		}
	}

	public function actionAsyncto($bDownload = true, $bImport = true, $iCap = null, $bVerbose = false)
	{
		if($bDownload)
			$bDownload == "true" ? $bDownload = true : $bDownload = false;
		if($bImport)
			$bImport == "true" ? $bImport = true : $bImport = false;
		if($iCap)
			$iCap = intval($iCap) ? intval($iCap) : null;
		if($bVerbose)
			$bVerbose == "true" ? $bVerbose = true : $bVerbose = false;
		$this->bVerbose = $bVerbose;

 		if($this->bVerbose)
			echo "\n".date("Y-m-d H:i:s")." - Updating Asyncto records\n";
		Asyncto::Model()->download($bDownload, $bImport, $iCap, $bVerbose);
	}

	public function actionSendReminder($bReallySend=false, $bVerbose=false)
	{
		if($bVerbose)
			$bVerbose == "true" ? $bVerbose = true : $bVerbose = false;
		$this->bVerbose = $bVerbose;

 		if($this->bVerbose)
			echo "\n".date("Y-m-d H:i:s")." - Sending Reminder\n";


        $criteria = new CDbCriteria();
        $criteria->condition = "sendingDate IS NULL";
        $criteria->limit = Lookup::item('sparkpost','batch_size');
        $criteria->order = "creationDate ASC";
        $models = Mailqueue::model()->findAll($criteria);
        if(is_array($models) && count($models) > 0)
        {
	        foreach($models as $model)
	        {
	            if($this->bVerbose)
	    			echo "\nMailqueue message #".$model->id." to ".$model->email."\n";
	            $model->send($bReallySend);
	            if($this->bVerbose)
	    			echo Lookup::item('mailqueue_sendingresult',$model->sendingresult_id).": ".$model->sendingresult;
	        }
        }
        else
        {
            echo "\nNothing to send\n";
        }
 		if($this->bVerbose)
			echo "\n".date("Y-m-d H:i:s")." - Done sending\n";
    }
}
?>