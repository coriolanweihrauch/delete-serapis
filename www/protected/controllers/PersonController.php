<?php

class PersonController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('ajaxItem','ajaxEmail'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('view','create','update','admin'),
				'roles'=>array('masteradmin','admin','staff'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('erase'),
				'roles'=>array('masteradmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
  	$model = $this->loadModel($id);

    $this->render('view',array(
        'model'=>$model
    ));
  }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Person;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Person']))
		{
			$model->attributes=$_POST['Person'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Person successfully created!");
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,

		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Person']))
		{
			$model->attributes=$_POST['Person'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Person successfully saved!");
// 				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionErase($id)
	{
		$model = $this->loadModel($id);

		$error = null;
		try
		{
			foreach($model->submeterConctact as $cc)
			{
				$cc->contact_id = null;
				$cc->save(false);
			}
			foreach($model->submetersLink as $cs)
			{
				$cs->delete();
			}
			$model->delete();
		}
		catch(Exception $e) {
		  $error = $e->getMessage();
		}

		if($error && isset($_GET['ajax']))
			echo CJavaScript::jsonEncode(array('error'=>$error));

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			if($error)
				Yii::app()->user->setFlash('error', $error);
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	$this->pageTitle = "Users";
		$model=new Person('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Person']))
			$model->attributes=$_GET['Person'];

    if($this->isExportRequest()) { //<==== [[ADD THIS BLOCK BEFORE RENDER]]
      //set_time_limit(0); //Uncomment to export lage datasets
      //Add to the csv a single line of text
/*       $this->exportCSV(array('POSTS WITH FILTER:'), null, false); */
      //Add to the csv a single model data with 3 empty rows after the data
/*       $this->exportCSV($model, array_keys($model->attributeLabels()), false, 3); */
      //Add to the csv a lot of models from a CDataProvider
      $this->exportCSV($model->search(), array('asynctoId', 'masterlistId', 'aurovillename', 'name', 'surname', 'community', 'email', 'telephone'));
    }

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Person the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Person::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Person $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='person-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	// data provider for EJuiAutoCompleteFkField for aurovilleName field
	public function actionAjaxItem($term)
	{
		$items=array();
		$models=Person::model()->findAll(array(
			'condition'=>"(aurovillename like :term OR name like :term OR surname like :term OR id like :term ) AND (deletionDate IS NULL or deletionDate = '0000-00-00 00:00:00')",
			'params'=>array(':term'=>$term."%"),
			'order'=>'aurovillename',
		));

		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->summary,
					'value' => $m->summary,
					'id' => $m->id, // return value from autocomplete
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
	}

	// data provider for EJuiAutoCompleteFkField for aurovilleName field
	public function actionAjaxEmail($id)
	{
		$items=array();
		$model=Person::model()->findByPk($id);

		if (!empty($model)) {
			$out = array();
			foreach ($model->emails as $e)
				$out['email'][] = $e->contact;
			echo CJSON::encode($out);
			Yii::app()->end();
		}
	}

	public function behaviors()
	{
    return array(
	    'exportableGrid' => array(
	      'class' => 'application.components.ExportableGridBehavior',
	      'filename' => 'emp_People_'.date('Y-m-d').'.csv',
	      'csvDelimiter' => ';', //i.e. Excel friendly csv delimiter
	    )
	  );
	}


	public function actionAsyncto()
	{
		$asyncto = new Asyncto();
		$asyncto->test();
	}

}