<?php

class SettingController extends MiAController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('batch'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('print','merge','cache'),
				'roles'=>array('admin', 'masteradmin',),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('erase',),
//				'users'=>array('admin'),
				'roles'=>array('masteradmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$this->redirect(Yii::app()->getBaseUrl().'/book/admin');
	}


	public function actionPrint()
	{
		$model = new Labelprint();
		$activeTab = $_POST['activeTab'];
		if(isset($_POST['Labelprint']))
		{
			$model->attributes=$_POST['Labelprint'];
			switch($activeTab)
			{
				case "tab_date":
					$model->barcode_from   = null;
					$model->barcode_to     = null;
					$model->barcodes       = null;
				break;
				case "tab_number":
					$model->date_from      = null;
					$model->date_to        = null;
					$model->barcodes       = null;
				break;
				case "tab_individual":
					$model->date_from      = null;
					$model->date_to        = null;
					$model->barcode_from   = null;
					$model->barcode_to     = null;
				break;
			}

			$book = new Book;
			if($model->date_from && $model->date_to)
				$book->search_creationDate = $model->date_from." - ".$model->date_to;
			$book->search_sn_from = $model->barcode_from;
			$book->search_sn_to = $model->barcode_to;
			if($model->barcodes)
				$book->search_sn_array = preg_split("/[\s,]+/", trim($model->barcodes));
			$book->checkoutCount = null;
			$book->deletionDate = "null";

			if($_POST['bExport'] == 1)
			{
				// Load data
				$dp = $book->search();
				$dp->setPagination(false);

				// Excel formatting
                $e = new PHPExcel();
				$e->getActiveSheet()->setTitle('labels');
				$e->setActiveSheetIndex(0);
				$iMaxCol = Lookup::item('excel','columns');
				$arrAlphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
				for($i=0; $i<$iMaxCol; $i++)
				{
					$e->getActiveSheet()->getColumnDimension($arrAlphabet[$i])->setWidth(Lookup::item('excel','width'));
				}
				$e->getActiveSheet()->getRowDimension(1)->setRowHeight(Lookup::item('excel','height'));

// check with jürgen
//                 $pageMargins = $e->getActiveSheet()->getPageMargins()->setTop('.25')->setBottom('.25')->setLeft('.2')->setRight('.2');

                $arrRecords = array();

				// Export Spine labels
				if($model->format_id == Labelprint::FORMAT_SPINE)
				{
					foreach($dp->getData() as $record)
					{
                        $arrRecords[$record->sn] = $record;
                    }
					$iRowIndex = 0;
                    $i = 0;
                    foreach($arrRecords as $record)
                    {
						if($i % $iMaxCol == 0)
							$iRowIndex++;
						$e->getActiveSheet()->getRowDimension($iRowIndex+1)->setRowHeight(Lookup::item('excel','height'));
						$e->getActiveSheet()->setCellValue($arrAlphabet[$i%$iMaxCol].($iRowIndex), trim($record->ddc."\n".$record->number."\n".$record->barcode));
						$e->getActiveSheet()->getStyle($arrAlphabet[$i%$iMaxCol].($iRowIndex))->getAlignment()->setWrapText(true);
                        $e->getActiveSheet()->getStyle($arrAlphabet[$i%$iMaxCol].($iRowIndex))->getFont()->setName(Lookup::item('excel','fontface'))->setSize(Lookup::item('excel','fontsize'))->setBold(true);
						$i++;
					}
                    $filename = "Serapis_labels_spine_".date("Ymj_Hi").".xls";
				}
				// Export barcode
				elseif($model->format_id == Labelprint::FORMAT_BARCODE)
				{
					foreach($dp->getData() as $record)
					{
                        $arrRecords[$record->sn] = $record;
                    }
					$iRowIndex = 0;
                    $i = 0;
                    foreach($arrRecords as $record)
                    {
						if($i % $iMaxCol == 0)
							$iRowIndex++;
						$e->getActiveSheet()->getRowDimension($iRowIndex+1)->setRowHeight(Lookup::item('excel','height'));
						$e->getActiveSheet()->setCellValue($arrAlphabet[$i%$iMaxCol].($iRowIndex), "*".$record->barcode."*");
						$e->getActiveSheet()->getStyle($arrAlphabet[$i%$iMaxCol].($iRowIndex))->getFont()->setName(Lookup::item('excel','barcodefont'))->setSize(Lookup::item('excel','barcodesize'));

						$i++;
                    }
                    $filename = "Serapis_labels_barcode_".date("Ymj_Hi").".xls";
				}

            // Create file & finish
		    ob_end_clean();
		    ob_start();
		    header('Content-Type: application/vnd.ms-excel');
		    header('Content-Disposition: attachment;filename="'.$filename.'"');
		    header('Cache-Control: max-age=0');
		    $objWriter = PHPExcel_IOFactory::createWriter($e, 'Excel5');
		    $objWriter->save('php://output');
				Yii::app()->end();
			}
		}

		$this->render('print',array(
			'model' => $model,
			'book' => $book,
			'activeTab' => $activeTab,
		));
	}

    public function actionBatch()
    {
        $model = new Labelprint();
		if(isset($_POST['Labelprint']))
		{
			$model->attributes=$_POST['Labelprint'];
			$arrBarcodes = preg_split("/[\s,]+/", trim($model->barcodes));
            $criteria = new CDbCriteria;
            $criteria->addInCondition('sn', $arrBarcodes);
            $arrBooks = Book::model()->findAll($criteria);

            $iCount = 0;
            foreach($arrBooks as $book)
            {
                set_time_limit(10);
                if(($key = array_search($book->sn, $arrBarcodes)) !== false) {
                    unset($arrBarcodes[$key]);
                }
                $book->status_id = $model->batch_status;
                $book->save(false);
                unset($book);
                $iCount++;
            }
            if(count($arrBarcodes))
            {
                Yii::app()->user->setFlash('warning', "Failed to update status for invalid barcodes: ".implode(", ", $arrBarcodes)."<br/> Successfully updated for ".$iCount." other books");
            }
            else
                Yii::app()->user->setFlash('success', "Updated the status for ".$iCount." books");
		}

		$model = new Labelprint();
		$this->render('batch',array(
			'model' => $model,
		));
    }

  public function actionExcel(){
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Hello')
    ->setCellValue('B2', 'world!')
    ->setCellValue('C1', 'Hello')
    ->setCellValue('D2', 'world!');

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A4', 'Miscellaneous glyphs')
    ->setCellValue('A5', 'eaeuaeioueiuyaouc');

    $objPHPExcel->getActiveSheet()->setTitle('Simple');

    $objPHPExcel->setActiveSheetIndex(0);

    ob_end_clean();
    ob_start();

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="test.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  }

  public function actionMerge()
  {
    $model = new Setting();
		$activeTab = $_POST['activeTab'];
		$model->attributes=$_POST['Setting'];

		switch($activeTab)
		{
			case "tab_author":
				$model->merge_keyword_from_id       = null;
				$model->merge_keyword_to_id         = null;
				$model->merge_topicalterm_from_id   = null;
				$model->merge_topicalterm_to_id     = null;

        if($model->merge_author_from_id && $model->merge_author_to_id)
        {
          $author_from  = Author::model()->findByPk($model->merge_author_from_id);
          $author_to    = Author::model()->findByPk($model->merge_author_to_id);
          if( ! $author_from)
    				Yii::app()->user->setFlash('error', "Invalid Author From");
          elseif( ! $author_to)
    				Yii::app()->user->setFlash('error', "Invalid Author From");
          else
          {
            $author_from->mergeOnto($author_to->id);
            $summary = $author_from->summary;
            $author_from->delete();
            Yii::app()->user->setFlash('success', "Deleted Author '".$summary."' and merged all books onto '".$author_to->summary."'");
          }
        }
        else
        {
  				Yii::app()->user->setFlash('error', "Please select Author From and To");
        }
			break;

			case "tab_keyword":
				$model->merge_author_from_id        = null;
				$model->merge_author_to_id          = null;
				$model->merge_topicalterm_from_id   = null;
				$model->merge_topicalterm_to_id     = null;

        if($model->merge_keyword_from_id && $model->merge_keyword_to_id)
        {
          $keyword_from  = Keyword::model()->findByPk($model->merge_keyword_from_id);
          $keyword_to    = Keyword::model()->findByPk($model->merge_keyword_to_id);
          if( ! $keyword_from)
    				Yii::app()->user->setFlash('error', "Invalid Keyword From");
          elseif( ! $keyword_to)
    				Yii::app()->user->setFlash('error', "Invalid Keyword From");
          else
          {
            $keyword_from->mergeOnto($keyword_to->id);
            $summary = $keyword_from->summary;
            $keyword_from->delete();
            Yii::app()->user->setFlash('success', "Deleted Keyword '".$summary."' and merged all books onto '".$keyword_to->summary."'");
          }
        }
        else
        {
  				Yii::app()->user->setFlash('error', "Please select Keyword From and To");
        }
			break;

			case "tab_topicalterm":
				$model->merge_author_from_id        = null;
				$model->merge_author_to_id          = null;
				$model->merge_keyword_from_id       = null;
				$model->merge_keyword_to_id         = null;

        if($model->merge_topicalterm_from_id && $model->merge_topicalterm_to_id)
        {
          $topicalterm_from  = Topicalterm::model()->findByPk($model->merge_topicalterm_from_id);
          $topicalterm_to    = Topicalterm::model()->findByPk($model->merge_topicalterm_to_id);
          if( ! $topicalterm_from)
    				Yii::app()->user->setFlash('error', "Invalid Topicalterm From");
          elseif( ! $topicalterm_to)
    				Yii::app()->user->setFlash('error', "Invalid Topicalterm From");
          else
          {
            $topicalterm_from->mergeOnto($topicalterm_to->id);
            $summary = $topicalterm_from->summary;
            $topicalterm_from->delete();
            Yii::app()->user->setFlash('success', "Deleted Topicalterm '".$summary."' and merged all books onto '".$topicalterm_to->summary."'");
          }
        }
        else
        {
  				Yii::app()->user->setFlash('error', "Please select Topicalterm From and To");
        }

			break;
		}

		$this->render('merge',array(
  		'model' => $model,
  		'activeTab' => $activeTab,
		));
  }

  public function actionCache($id=null){

    if($id){
        ini_set(max_execution_time, 600); // timeout to 10m
        switch($id){
            case Setting::CACHE_AUTHOR:
                $iCount = Setting::rebuildAuthorCache();
                Yii::app()->user->setFlash('success', "Updated Author caches for ".$iCount." books");
            break;
            default:
  				Yii::app()->user->setFlash('error', "Unknown cache");
            break;
        }
    }

    $this->render('cache');
  }

}