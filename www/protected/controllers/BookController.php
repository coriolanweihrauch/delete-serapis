<?php

class BookController extends MiAController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','search','view','listNew','listScores','excel'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('checkout','checkin','extend','ajaxPubPublisher','ajaxPubPlace','ajaxSeriesName', 'ajaxAgeGroup'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','delete','print','duplicate'),
				'roles'=>array('admin', 'masteradmin',),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('erase',),
//				'users'=>array('admin'),
				'roles'=>array('masteradmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Book;
		$model->sn = $model->findNextSn();
		$model->status_id = Book::STATUS_INCOMING;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Book']))
		{
			$model->attributes=$_POST['Book'];

			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Book successfully created!");
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionView($id)
	{
    	$this->pageTitle = "View";
		$model=$this->loadModel($id);
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$this->pageTitle = "Update";
		$model=$this->loadModel($id);

/*
print_r($model);
die();
*/

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Book']))
		{
			$model->attributes=$_POST['Book'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Book successfully saved!");
// 				$this->redirect(array('admin'));
			}
		}

        if($model->form_duplicate == '1')
            $this->redirect(array('duplicate','id'=>$model->id));


		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Marks a particular model as deleted
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	$this->pageTitle = "Search";
		$model=new Book('search');
		$model->unsetAttributes();  // clear any default values
		if(Yii::app()->user->isGuest)
    		$model->deletionDate = "null";
		if(isset($_GET['Book']))
			$model->attributes=$_GET['Book'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionListNew()
	{
    	$this->pageTitle = "New";
		$model=new Book('search');
		$model->unsetAttributes();  // clear any default values
		$model->search_creationDate = (new DateTime("today -1 months"))->format('Y-m-d')." - ".(new DateTime("today"))->format('Y-m-d');
		if(Yii::app()->user->isGuest)
    		$model->deletionDate = "null";
		if(isset($_GET['Book']))
			$model->attributes=$_GET['Book'];

		$this->render('listNew',array(
			'model'=>$model,
		));
	}

  public function actionSearch()
  {
    $this->pageTitle = "Adv Search";
	$activeTab = $_GET['activeTab'];

	$model=new Book('search');
// 		$model->deletionDate = "null";
	$model->checkoutCount = null;
	$model->unsetAttributes();
    $bHideResults = true;

/*
if(count($_GET) || count($_POST))
{
    print_r($_GET);
    print("<hr>");
    print_r($_POST);
    die();
}
*/

    // Posted parameters
    if(isset($_GET['Book']) && isset($_GET['Book']['search_advanced_pre']))
    {
        $model->attributes=$_GET['Book'];
        $bHideResults = false;
    }
    // No post - use session variables
    else
    {
/*
print_r(Yii::app()->session);
die();
*/
      if(Yii::app()->session['search_advanced_pre'])
      {
        $model->search_advanced_pre = Yii::app()->session['search_advanced_pre'];
        // bHideResult is subject to $pre being set
      }
      if(Yii::app()->session['search_advanced_multiple'])
      {
        $model->search_advanced_multiple = Yii::app()->session['search_advanced_multiple'];
        $bHideResults = false;
      }
      if(Yii::app()->session['search_advanced_multiple_fields'])
      {
        $model->search_advanced_multiple_fields = Yii::app()->session['search_advanced_multiple_fields'];
        $bHideResults = false;
      }
      if(Yii::app()->session['search_advanced_custom_fields'])
      {
        $model->search_advanced_custom_fields = Yii::app()->session['search_advanced_custom_fields'];
        $bHideResults = false;
      }
    }
    // Make sure custom fields are ticked to defaults (less work for user)
    if( ! $model->search_advanced_custom_fields)
    {
      $model->search_advanced_custom_fields = $model->defaultSearchAttributes;
    }

    // pre-search filter submitted from corresponding cgridview
    if(isset($_REQUEST['Book']['search_advanced_pre_model'])
      && isset($_REQUEST['Book']['search_advanced_pre_model']['category'])
      && isset($_REQUEST['Book']['search_advanced_pre_model']['id'])
    )
    {
        if($_REQUEST['Book']['search_advanced_pre_model']['category'] != "" && $_REQUEST['Book']['search_advanced_pre_model']['id'] != "")
        {
            $preName = ucfirst($_REQUEST['Book']['search_advanced_pre_model']['category']);
            $pre =  new $preName;
            $model->search_advanced_pre_model = $pre::model()->findByPk($_REQUEST['Book']['search_advanced_pre_model']['id']);
        }
        else
        {
            $model->search_advanced_pre_model = null;
            Yii::app()->session['search_advanced_pre_model_category']   = null;
            Yii::app()->session['search_advanced_pre_model_id']         = null;
        }
    }
    elseif(Yii::app()->session['search_advanced_pre_model_category'] && Yii::app()->session['search_advanced_pre_model_id'])
    {
      $preName = ucfirst(Yii::app()->session['search_advanced_pre_model_category']);
      $pre =  new $preName;
      $model->search_advanced_pre_model = $pre::model()->findByPk(Yii::app()->session['search_advanced_pre_model_id']);
    }
    else
    {
      $model->search_advanced_pre_model = null;
    }

    // CGridView attributes come by $_GET
		if(isset($_GET['Book']))
		{
  		// Only CGridView search
          if($bHideResults)
          {
      			$model->attributes=$_GET['Book'];
          }
  		// Already top search form
  		else
  		{
    	  $modelGet = new Book('search');
  			$modelGet->attributes=$_GET['Book'];
  			foreach($modelGet->attributes as $attr => $val)
  			{
  			  if( ! $model->{$attr})
  			    $model->{$attr} = $val;
  			}
  		}
      $bHideResults = false;
		}

    if($activeTab)
    {
      switch($activeTab)
      {
        case "tab_pre":
          $model->search_advanced_multiple          = null;
          $model->search_advanced_multiple_fields   = null;
          // If no pre-search item has been selected, hide search results;
        break;
        case "tab_multiple":
          $model->search_advanced_pre               = null;
          if(isset($_GET['Book']['search_advanced_pre']))
            $_GET['Book']['search_advanced_pre']    = null;
        break;
        case "tab_custom":
          // Do nothing as this applies columsn for the other 2 tabs
          $activeTab = $_GET['Book']['search_advanced_pre'] != "" ? "tab_pre" : "tab_multiple";
        break;
      }
      // Remove remnant search_advanced_pre_model when using tab multiple
      // (also via tab_custom redirecting here - hence, outside the switch(){}
      if($activeTab == "tab_multiple")
      {
        $model->search_advanced_pre_model                               = null;
        if(isset($_REQUEST['Book']['search_advanced_pre_model']['category'] ))
          $_REQUEST['Book']['search_advanced_pre_model']['category']    = null;
        if(isset($_REQUEST['Book']['search_advanced_pre_model']['id'] ))
          $_REQUEST['Book']['search_advanced_pre_model']['id']          = null;
        Yii::app()->session['search_advanced_pre_model_category']       = null;
        Yii::app()->session['search_advanced_pre_model_id']             = null;
      }
    }

// die($model->search_advanced_pre_model);

    // Create pre-search results models
/*
    if($_GET['Book']['search_advanced_pre'] && !$_REQUEST['Book']['search_advanced_pre_model']['category'] && !$_REQUEST['Book']['search_advanced_pre_model']['id']
      || $_GET['Author'] || $_GET['Keyword'] || $_GET['Topicalterm']
    )
*/
    if($_GET['Book']['search_advanced_pre'] && !$model->search_advanced_pre_model
      || $_GET['Author'] || $_GET['Keyword'] || $_GET['Topicalterm']
    )
    {

/*
print_r($model);
die();
*/


//                 die('p');
        // Disabled option: POST requests do not work well with pagination
        if(is_array($model->search_advanced_pre_paginate) && $model->search_advanced_pre_paginate[0] == '0')
            $paginationOff = true;
        else
            $paginationOff = false;
//         $paginationOff = true;

  		$author=new Author('search');
  		$author->unsetAttributes();  // clear any default values
  		$author->_paginationOff = $paginationOff;
        $author->name = $model->search_advanced_pre;
  		if(isset($_GET['Author']))
  			$author->attributes=$_GET['Author'];

  		$keyword=new Keyword('search');
  		$keyword->unsetAttributes();  // clear any default values
  		$keyword->_paginationOff = $paginationOff;
        $keyword->name = $model->search_advanced_pre;
  		if(isset($_GET['Keyword']))
  			$author->attributes=$_GET['Keyword'];

  		$topicalterm=new Topicalterm('search');
  		$topicalterm->unsetAttributes();  // clear any default values
  		$topicalterm->_paginationOff = $paginationOff;
        $topicalterm->name = $model->search_advanced_pre;
  		if(isset($_GET['Topicalterm']))
  			$topicalterm->attributes=$_GET['Topicalterm'];

      // clear model as we're searching for a new one
      $model->search_advanced_pre_model = null;
    }
    if( ! $model->search_advanced_pre_model && ! $model->search_advanced_multiple)
      $bHideResults = true;

    // Set session variables (always set to make sure null gets set)
    if($model->search_advanced_pre_model)
    {
      Yii::app()->session['search_advanced_pre_model_category'] = strtolower(get_class($model->search_advanced_pre_model));
      Yii::app()->session['search_advanced_pre_model_id']       = $model->search_advanced_pre_model->id;
    }

    Yii::app()->session['search_advanced_pre']                  = $model->search_advanced_pre;
    Yii::app()->session['search_advanced_multiple']             = $model->search_advanced_multiple;
    Yii::app()->session['search_advanced_multiple_fields']      = $model->search_advanced_multiple_fields;
    Yii::app()->session['search_advanced_custom_fields']        = $model->search_advanced_custom_fields;

		if($bHideResults)
      $model->id = 0;

		$this->render('search',array(
			'model'=>$model,
			'activeTab'=>$activeTab,
			'author' => $author,
			'keyword' => $keyword,
			'topicalterm' => $topicalterm,
			'bHideResults' => $bHideResults,
		));
  }

	public function actionListScores()
	{
    	$this->pageTitle = "Scores";
		$model=new Book('search');
		$model->unsetAttributes();  // clear any default values
		if(Yii::app()->user->isGuest)
    		$model->deletionDate = "null";
		if(isset($_GET['Book']))
			$model->attributes=$_GET['Book'];

		$this->render('listScores',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages deleted models.
	 */
	public function actionAdminTrash()
	{
		$model=new Book('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Book']))
			$model->attributes=$_GET['Book'];

		$this->render('admin',array(
			'model'=>$model,
			'istrash'=>true,
		));
	}

    public function actionDuplicate($id)
    {
		$model=$this->loadModel($id);
		$newBook = new Book();
		$newBook->setAttributes($model->attributes);
		$newBook->sn = $newBook->findNextSN();

        $newBook->save(false);

        foreach($model->link_author as $link_author)
        {
            $ab = new Author_Book;
            $ab->book_id    = $newBook->id;
            $ab->author_id  = $link_author->author_id;
            $ab->role_id    = $link_author->role_id;
            $ab->save(false);
        }

        foreach($model->topicalterms as $topicalterm)
        {
            $bt = new Book_Topicalterm;
            $bt->book_id        = $newBook->id;
            $bt->topicalterm_id = $topicalterm->id;
            $bt->save(false);
        }

        foreach($model->features as $feature)
        {
            $bf = new Book_Feature;
            $bf->book_id    = $newBook->id;
            $bf->feature_id = $feature->id;
            $bf->save(false);
        }

        foreach($model->keywords as $keyword)
        {
            $bk = new Book_Keyword;
            $bk->book_id    = $newBook->id;
            $bk->keyword_id = $keyword->id;
            $bk->save(false);
        }

		Yii::app()->user->setFlash('success', "Book successfully duplicated!");
		$this->redirect(array('update','id'=>$newBook->id));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Activity the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Book::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Activity $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Book-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	// data provider for EJuiAutoCompleteFkField for Author > notes field
	public function actionAjaxPubPublisher($term)
	{
		$models=Book::model()->findAll(array(
    		'select' => 'pubPublisher',
			'condition'=>'pubPublisher like :term',
			'params'=>array(':term'=>$term."%"),
			'order'=>'pubPublisher',
			'group' => 'pubPublisher'
		));

		$arrIds = array();
		if(count($models))
			foreach($models as $m)
				$arrIds[] = $m->id;


		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->pubPublisher,
					'value' => $m->pubPublisher,
					'id' => $m->id, // return value from autocomplete
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
    }

	// data provider for EJuiAutoCompleteFkField for Author > notes field
	public function actionAjaxPubPlace($term)
	{
		$models=Book::model()->findAll(array(
    		'select' => 'pubPlace',
			'condition'=>'pubPlace like :term',
			'params'=>array(':term'=>$term."%"),
			'order'=>'pubPlace',
			'group' => 'pubPlace'
		));

		$arrIds = array();
		if(count($models))
			foreach($models as $m)
				$arrIds[] = $m->id;


		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->pubPlace,
					'value' => $m->pubPlace,
					'id' => $m->id, // return value from autocomplete
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
    }

	// data provider for EJuiAutoCompleteFkField for Author > notes field
	public function actionAjaxSeriesName($term)
	{
		$models=Book::model()->findAll(array(
    		'select' => 'seriesName',
			'condition'=>'seriesName like :term',
			'params'=>array(':term'=>$term."%"),
			'order'=>'seriesName',
			'group' => 'seriesName'
		));

		$arrIds = array();
		if(count($models))
			foreach($models as $m)
				$arrIds[] = $m->id;


		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->seriesName,
					'value' => $m->seriesName,
					'id' => $m->id, // return value from autocomplete
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
    }

	// data provider for EJuiAutoCompleteFkField for Author > notes field
	public function actionAjaxAgeGroup($term)
	{
		$models=Book::model()->findAll(array(
    		'select' => 'ageGroup',
			'condition'=>'ageGroup like :term',
			'params'=>array(':term'=>$term."%"),
			'order'=>'ageGroup',
			'group' => 'ageGroup'
		));

		$arrIds = array();
		if(count($models))
			foreach($models as $m)
				$arrIds[] = $m->id;


		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->ageGroup,
					'value' => $m->ageGroup,
					'id' => $m->id, // return value from autocomplete
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
    }

}