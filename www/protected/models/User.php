<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the availabel columns in table 'user':
 * @property integer $id
 * @property integer $building_id
 * @property integer $type_id
 * @property string $email
 * @property string $password
 * @property string $lastname
 * @property string $position
 * @property string $employeeNumber
 * @property string $profile
 * @property string $phone
 * @property string $mobile
 * @property string $dateCreated
 *
 * The followings are the availabel model relations:
 * @property Reading[] $readings
 * @property Area $area
 * @property Site $site
 * @property User $updatedBy
 * @property User[] $users
 */
class User extends MiACActiveRecord
{
	const TYPE_USER        = 1;
	const TYPE_DISABLED    = 2;
	const TYPE_ADMIN       = 3;
	const TYPE_MASTERADMIN = 4;
	const TYPE_CLIENT      = 5;

	const MAX_ACTIVATIONMAIL_COUNT = 3;
	const MAX_RESETMAIL_COUNT = 3;

	const MAX_RESETMAIL_LOGIN_INTERVAL_DAYS = 2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			// register
			array('email, password, password_repeat', 'required', 'on'=>'register'),
			array('email, password, password_repeat', 'safe', 'on'=>'register'),
			array('password_repeat', 'compare', 'compareAttribute'=>'password' , 'on'=>'register', 'message'=>'Passwords do not match'),

			// activate
			array('email, activationcode_temp', 'required', 'on'=>'activate'),
			array('email, activationcode_temp', 'safe', 'on'=>'activate'),

			// reset password
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on' =>'forgotPassword'),
/*
			array('email', 'required', 'on'=>'activate'),
			array('email', 'safe', 'on'=>'activate'),
*/

			// Admin update
			array('email', 'required', 'on'=>'update'),
			array('resetPassword', 'safe', 'on'=>'update'),


			// Manager profile
			array('email, password, name', 'required', 'on'=>'myProfile'),

			array('type_id, name,email', 'required'),

			// general
			array('email', 'unique', 'on'=>'register,create,update'),
			array('activationcode,resetcode', 'length', 'min'=>'32', 'max'=>'32', 'tooLong'=>'Invalid code', 'tooShort'=>'Invalid code'),
			array('type_id, background_id', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>128),
			array('email','email'),
			array('password, name', 'length', 'max'=>64),
			array('newPassword', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type_id, email, password, name, activationcode_temp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
/* 			'areas' => array(self::MANY_MANY, 'Area', 'area_manager(area_id,manager_id)'), */
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type_id' => 'Type',
			'background_id' => 'Background',
			'email' => 'Email',
			'password' => 'Password',
    	    'password_repeat'=>'Repeat password',
    	    'activationcode' => 'Activation code',
    	    'activationcode_temp' => 'Activation code',
    	    'resetcode' => 'Password reset code',
			'resetPassword' => 'Password',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public $password_repeat;
	public $newPassword;
	public $resetPassword;
	public $verifyCode;
	public $activationcode_temp;
	public $resetcode_temp;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);

		$criteria->compare('name',$this->name,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size'),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getTypeText()
	{
		return Lookup::item('user_type', $this->type_id);
	}

	public function getSiteCount()
	{
		$c = 0;
		foreach($this->areas as $a)
			$c += count($a->sites);
		return $c;
	}

	public function getAreaCount()
	{
		return count($this->areas);
	}

	public function lookupList($section)
	{
		$list = Lookup::items($section);
		$cr = array();
		foreach($list as $i => $v)
			$cr[] = array("id" => $i, "title" => $v);
		return $cr;
	}

	public function encryptPassword()
	{
// 		$this->password = crypt($this->newPassword,$this->newPassword);
		$this->password = password_hash($this->newPassword, PASSWORD_DEFAULT);
	}

	public function getRandomCode()
	{
// 		PHP 7
// 		return random_bytes(32);
//    PHP 5.3
		return bin2hex(openssl_random_pseudo_bytes(16));
	}

	public function resetPassword()
	{
		$this->newPassword = $this->getRandomCode();
		$this->password = password_hash($this->newPassword, PASSWORD_DEFAULT);
		$this->sendActivationmail(true);
	}

	public function sendActivationmail($sendImmediately=false)
	{
// 		return array('status'=>'warning', 'message'=>'sent activation email to '.$this->email)	;
		$mq = new	Mailqueue;
		$mq->user_id = $this->id;
		$mq->email = $this->email;
		$mq->template_id = Mailqueue::TEMPLATE_ACTIVATION;
		$mq->mergevariables = array(
			'activationurl' => Yii::app()->createAbsoluteUrl('user/activate', array('email'=>$this->email,'activationcode'=>$this->activationcode)),
			'recipient_name' => $this->name ? $this->name : $this->email,
		);
		if( ! $mq->save())
			return array('status'=>'error', 'message'=>'Error adding email to send queue');
		else
		{
			if($sendImmediately)
			{
				// Decode JSON (due to save)
				$mq->afterFind();
				$mq->send(true);
			}
			$this->activationmail_count++;
			$this->save(false);
			return array('status'=>'success', 'message'=>'Please check your email to find the activation link / code');
		}
	}

	public function sendWelcomemail($sendImmediately=false)
	{
		$mq = new	Mailqueue;
		$mq->user_id = $this->id;
		$mq->email = $this->email;
		$mq->template_id = Mailqueue::TEMPLATE_WELCOME;
		$mq->mergevariables = array(
			'recipient_name' => $this->name,
		);
		if( ! $mq->save())
			return array('status'=>'error', 'message'=>'Error adding email to send queue');
		else
		{
			if($sendImmediately)
			{
				// Decode JSON (due to save)
				$mq->afterFind();
				$mq->send(true);
			}
			return array('status'=>'success', 'message'=>'Account activated - welcome to '.Yii::app()->name);
		}
	}

	protected function beforeSave()
	{
		if($this->isNewRecord)
			$this->encryptPassword();
	  return parent::beforeSave();
	}

	public function sendResetmail($sendImmediately=false)
	{
		$mq = new	Mailqueue;
		$mq->user_id = $this->id;
		$mq->email = $this->email;
		$mq->template_id = Mailqueue::TEMPLATE_RESETPASSWORD;
		$mq->mergevariables = array(
			'reseturl' => Yii::app()->createAbsoluteUrl('user/resetpassword', array('email'=>$this->email,'resetcode'=>$this->resetcode)),
			'recipient_name' => $this->name,
		);
		if( ! $mq->save())
			return array('status'=>'error', 'message'=>'Error adding email to send queue');
		else
		{
			if($sendImmediately)
			{
				// Decode JSON (due to save)
				$mq->afterFind();
				$r = $mq->send(true);
			}
			$this->resetmail_count++;
			$this->save(false);
			return array('status'=>'success', 'message'=>'Please check your email to find the activation link / code');
		}
	}

	public function getEmailFromId($id)
	{
		return User::model()->findByPk($id)->email;
	}

	public function getDisplayName()
	{
		return $this->name." <".$this->email.">";
	}

	public function getSummary()
	{
		return $this->getDisplayName();
	}

	public function showBadges()
	{
		print "here be badges";
	}

	public function getTypeBadge()
	{
		$label = $this->typeText;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('user_type_'.$label)));
 		return $this->makeBadge($type,$label);
	}

	private static $_teachers=array();

	public static function teachers()
	{
		if(!count(self::$_teachers))
			self::loadTeachers();
		return self::$_teachers;
	}

	private static function loadTeachers()
	{
		self::$_teachers=array();
		$models=self::model()->findAll(array(
			'condition'=>'type_id=:type_id',
			'params'=>array(':type_id'=>'5'),
			'order'=>'name',
		));
		foreach($models as $model)
			self::$_teachers[$model->id]=$model->name;
	}

}