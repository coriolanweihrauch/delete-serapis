<?php

/**
 * This is the model class for table "author".
 *
 * The followings are the available columns in table 'author':
 * @property string $id
 * @property string $title
 * @property string $name
 * @property string $nameFullerForm
 * @property string $lifetime
 * @property string $notes
 */
class Author extends MiACActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'author';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('title', 'length', 'max'=>32),
			array('name', 'length', 'max'=>128),
			array('nameFullerForm', 'length', 'max'=>255),
			array('lifetime', 'length', 'max'=>64),
			array('notes', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, name, nameFullerForm, lifetime, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'books' => array(self::MANY_MANY, 'Book', 'author_book(author_id,book_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'name' => 'Name',
			'nameFullerForm' => 'Name Fuller Form',
			'lifetime' => 'Lifetime',
			'notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
     var $_paginationOff;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
/*
		$criteria->compare('name',$this->name,true);
		$criteria->compare('nameFullerForm',$this->nameFullerForm,true);
*/
        if($this->name)
		{
            $arrAuthor = explode(" ", $this->name);
            foreach($arrAuthor as $i => $author)
                $arrAuthor[$i] = "+ ".$author;
            $criteria->addCondition("MATCH (t.name) AGAINST (\"".implode(" ",$arrAuthor)."\" IN BOOLEAN MODE)");
		}
        if($this->nameFullerForm)
		{
            $arrAuthor = explode(" ", $this->nameFullerForm);
            foreach($arrAuthor as $i => $author)
                $arrAuthor[$i] = "+ ".$author;
            $criteria->addCondition("MATCH (t.nameFullerForm) AGAINST (\"".implode(" ",$arrAuthor)."\" IN BOOLEAN MODE)");
		}

		$criteria->compare('lifetime',$this->lifetime,true);
		$criteria->compare('notes',$this->notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=> $this->_paginationOff || $_GET['showall'] == 1 ? false : array(
				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size'),
			),
		));
	}

	public function afterSave()
	{
        parent::afterSave();
        foreach($this->books as $book)
        {
            $book->updateAuthorNameCache();
            $book->save(false);
        }
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Author the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getSummary()
	{
		return $this->name;
	}

	public function getSummaryAutocomplete()
	{
		$strSummary = $this->name;
		if($this->lifetime)
			$strSummary .= " - ".$this->lifetime;
		if($this->notes)
			$strSummary .= " - ".$this->notesShort;
		return $strSummary;
	}

	public function getNotesShort()
	{
		if($this->notes)
			return substr($this->notes, 0, 10);
		else
			return null;
	}

	public function showBadges()
	{
// 		print "here be badges";
	}

  public function mergeOnto($idTo)
  {
    $strQuery = "UPDATE ".Author_Book::tableName()." SET `author_id` = '".$idTo."' WHERE `author_id` = '".$this->id."'";
    $r = Yii::app()->db->createCommand($strQuery)->query();
  }
}
