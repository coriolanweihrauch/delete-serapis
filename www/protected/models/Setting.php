<?php

/**
 * This is the model class for table "keyword".
 *
 * The followings are the available columns in table 'keyword':
 * @property string $id
 * @property string $name
 * @property string $notes
 */
class Setting extends CModel
{
	var $merge_author_to_id, $merge_author_from_id,
	$merge_keyword_to_id, $merge_keyword_from_id,
	$merge_topicalterm_to_id, $merge_topicalterm_from_id;

	var $merge_author_to, $merge_author_from,
	$merge_keyword_to, $merge_keyword_from,
	$merge_topicalterm_to, $merge_topicalterm_from;

    CONST CACHE_AUTHOR = 1;

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('merge_author_to_id, merge_author_from_id, merge_keyword_to_id, merge_keyword_from_id, merge_topicalterm_to_id, merge_topicalterm_from_id', 'length', 'max'=>11),
			array('', 'safe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
      'merge_author_to_id' => 'Autor To',
      'merge_author_from_id' => 'Author From',
      'merge_keyword_to_id' => 'Keyword To',
      'merge_keyword_from_id' => 'Keyword From',
      'merge_topicalterm_to_id' => 'Topical Term To',
      'merge_topicalterm_from_id' => 'Topical Term From',

      'merge_author_to' => 'Autor To',
      'merge_author_from' => 'Author From',
      'merge_keyword_to' => 'Keyword To',
      'merge_keyword_from' => 'Keyword From',
      'merge_topicalterm_to' => 'Topical Term To',
      'merge_topicalterm_from' => 'Topical Term From',
		);
	}

	public function attributeNames()
	{
		return $this->attributeLabels();
	}

    public static function rebuildAuthorCache()
    {
        return Book::resetAuthorNameCache();
    }
}