<?php

/**
 * This is the model class for table "contactdetail".
 *
 * The followings are the available columns in table 'contactdetail':
 * @property string $person_id
 * @property string $contactType_id
 * @property integer $bAsynctoDelete
 * @property string $contact
 *
 * The followings are the available model relations:
 * @property Person $person
 */
class Contactdetail extends CActiveRecord
{
	const TYPE_TELEPHONE = 1;
	const TYPE_EMAIL = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contactdetail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bAsynctoDelete', 'numerical', 'integerOnly'=>true),
			array('person_id, contactType_id', 'length', 'max'=>11),
			array('contact', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('person_id, contactType_id, bAsynctoDelete, contact', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'person_id' => 'User',
			'contactType_id' => 'Contact Type',
			'bAsynctoDelete' => 'Sync to ASyncTo',
			'contact' => 'Contact',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('person_id',$this->person_id,true);
		$criteria->compare('contactType_id',$this->contactType_id,true);
		$criteria->compare('bAsynctoDelete',$this->bAsynctoDelete);
		$criteria->compare('contact',$this->contact,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contactdetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getContactType()
	{
		return Lookup::item('contact_type', $this->contactType_id);
	}

	public function __toString()
	{
		return $this->contact;
	}
}
?>