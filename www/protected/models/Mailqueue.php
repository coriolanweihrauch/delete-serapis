<?php
use SparkPost\SparkPost;
use Ivory\HttpAdapter\CurlHttpAdapter;

function autoload($classId)
{
  $classIdParts       = explode("\\", $classId);
  $classIdLength      = count($classIdParts);
  $className          = $classIdParts[$classIdLength - 1];
  $namespace          = $classIdParts[0];
  $basedir						= Yii::app()->basePath."/vendor";

  for ($i = 1; $i < $classIdLength - 1; $i++) {
    $namespace .= '/' . $classIdParts[$i];
  }

  if ($basedir
    . '/' . $namespace
    . '/' . $className
    . '.php') {
    include $basedir . "/" . $namespace . '/' . $className . '.php';
  }
}

class Mailqueue extends MiACActiveRecord
{
	const TEMPLATE_ACTIVATION      = 1;
	const TEMPLATE_WELCOME         = 2;
	const TEMPLATE_RESETPASSWORD   = 3;

	const TEMPLATE_REMINDER_1      = 4;
	const TEMPLATE_REMINDER_2      = 5;
	const TEMPLATE_REMINDER_3      = 6;
	const TEMPLATE_REMINDER_4      = 7;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mailqueue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id,user_id,email,template_id,mergevariables,sendingDate,sendingresult,sendingresult_id,creationDate,createdBy_id,modificationDate,modifiedBy_id,deletedBy_id,deletionDate','safe','on'=>'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Person', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'template_id' => 'Template',
			'mergevariablesRich' => 'Merge Variables',
			'sendingresult_id' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('email',$this->email);
		$criteria->compare('template_id',$this->template_id);
		$criteria->compare('mergevariables',$this->mergevariables,true);
		$criteria->compare('sendingDate',$this->sendingDate,true);
		$criteria->compare('sendingresult_id',$this->sendingresult_id);
		$criteria->compare('sendingDate',$this->sendingDate, true);
		$criteria->compare('createdBy_id',$this->createdBy_id,true);
		$criteria->compare('creationDate',$this->creationDate,true);
		$criteria->compare('modifiedBy_id',$this->modifiedBy_id,true);
		$criteria->compare('modificationDate',$this->modificationDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size'),
			),
			'sort' => array(
		    'defaultOrder' => array(
	        't.creationDate' => CSort::SORT_DESC,
		    ),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Meter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getSummary()
	{
		return 'Mail Queue item '.$this->id;
	}

	public function showBadges()
	{
		return 'here be badges';
	}

	public function getTemplateBadge()
	{
		$label = Lookup::item('mailqueue_template', $this->template_id);
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('mailqueue_template_'.$label)));
 		return $this->makeBadge($type,$label);
	}

	public function getSendingresultBadge()
	{
// return 'mailqueue_sendingresult_'.Lookup::item('mailqueue_sendingresult',$this->sendingresult_id);
		$label = $this->sendingresult;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('mailqueue_sendingresult_'.Lookup::item('mailqueue_sendingresult',$this->sendingresult_id))));
 		return $this->makeBadge($type,$label);
	}

	public function getMergevariablesRich()
	{
		$strMV = '';
		if(is_array($this->mergevariables))
			foreach($this->mergevariables as $name=>$content)
			{
				$strMV .= "<b>".$name."</b>: ".$content."<br/>";
			}
		return $strMV;
	}

	public function beforeSave()
	{
		if($this->mergevariables)
			$this->mergevariables = CJSON::encode($this->mergevariables);
        return parent::beforeSave();
	}

	public function afterFind()
	{
		$this->mergevariables = CJSON::decode($this->mergevariables);
		return parent::afterFind();
	}

	public function getMergevariablesMailqueue()
	{
		$arrMV = array();
		if(is_array($this->mergevariables))
		{
			foreach($this->mergevariables as $name=>$content)
				$arrMV[] = array('name'=>$name, 'content'=>$content);
		}
		return $arrMV;
	}

	public function send($bReallySend=false, $bVerbose=false)
	{
		spl_autoload_register('autoload');


/*
		// Replaced by autoloader above !!
		Yii::setPathOfAlias('Sparkpost', Yii::app()->basePath."/vendor/SparkPost");
		Yii::setPathOfAlias('Ivory', Yii::app()->basePath."/vendor/Ivory");

		foreach (glob(Yii::app()->basePath."/vendor/SparkPost/*.php") as $filename)
		{
    	include $filename;
		}
		foreach (glob(Yii::app()->basePath."/vendor/Psr/Http/Message/*.php") as $filename)
		{
    	include $filename;
		}
		include_once(Yii::app()->basePath."/vendor/Zend/Dicarios/Uri.php");
		include_once(Yii::app()->basePath."/vendor/Zend/Dicarios/Stream.php");
		include_once(Yii::app()->basePath."/vendor/Zend/Dicarios/HeaderSecurity.php");
		include_once(Yii::app()->basePath."/vendor/Zend/Dicarios/RequestTrait.php");
		include_once(Yii::app()->basePath."/vendor/Zend/Dicarios/MessageTrait.php");
		include_once(Yii::app()->basePath."/vendor/Zend/Dicarios/Request.php");
		include_once(Yii::app()->basePath."/vendor/Zend/Dicarios/Response.php");
		include_once(Yii::app()->basePath."/vendor/Ivory/HttpAdapter/CurlHttpAdapter.php");
*/

		$sparky = new SparkPost(new CurlHttpAdapter(), Lookup::item('sparkpost','apikey'));
		$substituation_data = $this->mergevariables;
		$name = '';
		if($this->user)
		    $name = $this->user->aurovillename;
		$arrMsg = [
				'from'=>  Lookup::item('sparkpost','from_name') . " <".Lookup::item('sparkpost','from_email').">",
				'recipients'=>[
					[
						'address'=>[
							'email'=>$this->email,
							'name'=>$name,
						],
						'substitution_data'=>$substituation_data,
					]
				],
				'template'=>Lookup::item('mailqueue_template', $this->template_id),
			];
		if($bVerbose)
            print_r($arrMsg);
// 		$substituation_data = array_merge(array('sender_name' => 'console', 'recipient_name'=>$this->user->name), $this->mergevariables);
		try {
			// https://www.sparkpost.com/blog/using-sparkpost-in-php
			// https://github.com/SparkPost/php-sparkpost
			// https://developers.sparkpost.com/api/?_ga=1.3508720.1140756713.1461036052
			// https://support.sparkpost.com/customer/en/portal/articles/2140916-extended-error-codes
			$results = $sparky->transmission->send($arrMsg);
			if($bVerbose)
                print_r($results);
			// Successfully sent
			if($results['results']['total_rejected_recipients'] == 0 && $results['results']['total_accepted_recipients'] == 1 )
			{
				$this->sendingresult_id = 1;
			}
			else
			{
				$this->sendingresult_id = 2;
			}
			$this->sendingresult = $results['results']['id'];
			$this->sendingDate = date('Y-m-d H:i:s');
		}
		catch (Exception $error) {
			$results = [
				'api_code' => $error->getAPICode(),
				'api_message' => $error->getAPIMessage(),
				'api description' => $error->getAPIDescription(),
				'message' => $error->getMessage(),
				'code' => $error->getCode()
			];
			if($bVerbose)
                print_r($results);
			$this->sendingresult_id = 2;
			$this->sendingDate = date('Y-m-d H:i:s');
			$this->sendingresult = "error [".$results['api_code']."] ".$results['api_description'].$results['api_message'];
		}

		$this->save(false);
	}
}