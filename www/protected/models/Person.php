<?php

/**
 * This is the model class for table "Client".
 *
 * The followings are the available columns in table 'Client':
 * @property string $id
 * @property string $asynctoId
 * @property string $masterlistId
 * @property string $aurovillename
 * @property string $name
 * @property string $surname
 * @property string $dob
 * @property string $sex_id
 * @property string $nationality_id
 * @property string $status_id
 * @property integer $status_date
 * @property integer $familySize
 * @property string $presence_id
 * @property string $presence_date
 * @property string $community_id
 *
 * The followings are the available model relations:
 * @property Case[] $cases
 * @property Case[] $cases1
 * @property Case[] $cases2
 * @property Case[] $cases3
 * @property CaseDecision[] $caseDecisions
 * @property CaseDecision[] $caseDecisions1
 * @property CaseFeedback[] $caseFeedbacks
 * @property CaseFeedback[] $caseFeedbacks1
 * @property CaseFeedback[] $caseFeedbacks2
 * @property CaseFeedback[] $caseFeedbacks3
 * @property CaseFeedback[] $caseFeedbacks4
 * @property CaseFeedback[] $caseFeedbacks5
 * @property CaseClient[] $casePeople
 * @property Constructionevent[] $constructionevents
 * @property Constructionevent[] $constructionevents1
 * @property Constructionevent[] $constructionevents2
 * @property Contactdetail[] $contactdetails
 * @property Evaluation[] $evaluations
 * @property Grant[] $grants
 * @property Occupation[] $housingassetOccupants
 * @property Occupation[] $housingassetOccupants1
 * @property PaymentAgreement[] $paymentAgreements
 * @property PaymentAgreement[] $paymentAgreements1
 * @property Project[] $projects
 * @property Project[] $projects1
 * @property Project[] $projects2
 * @property TicketRequest[] $ticketRequests
 */
class Person extends MiACActiveRecord
{
	const OVERDUE_NONE     = 1;
	const OVERDUE_OVERDUE  = 2;
	const OVERDUE_DISABLED = 3;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('asynctoId', 'length', 'max'=>36),
			array('masterlistId', 'length', 'max'=>6),
			array('aurovillename', 'length', 'max'=>64),
			array('name', 'length', 'max'=>96),
			array('surname', 'length', 'max'=>32),
			array('workplace', 'length', 'max'=>255),
			array('creationDate, deletionDate, notes', 'safe'),
			array('community_id, status_id, category_id, presence_id, overdue_id, limitBooks, limitDays, limitExtensions', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, asynctoId, masterlistId, aurovillename, name, surname, workplace, community_id, status_id, category_id, presence_id, overdue_id, limitBooks, limitDays, limitExtensions', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'checkout' => array(self::HAS_MANY, 'Checkout', 'person_id'),
			'currentcheckout' => array(self::HAS_MANY, 'Checkout', 'person_id', 'condition'=>'returnDate = "0000-00-00"', 'order'=>'dueDate ASC'),
			'contactdetails' => array(self::HAS_MANY, 'Contactdetail', 'person_id'),
			'emails' => array(self::HAS_MANY, 'Contactdetail', 'person_id', 'condition' => 'contactType_id = 2'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'summary' => 'User',

			'id' => 'ID',
			'asynctoId' => 'Asyncto ID',
			'masterlistId' => 'Masterlist',
			'aurovillename' => 'Auroville Name',
			'name' => 'Name',
			'surname' => 'Surname',
			'workplace' => 'Work place',
			'address' => 'Address (old)',
			'community_id' => 'Community',
			'status_id' => 'Status',
			'category_id' => 'Status',
			'presence_id' => 'Presence',
			'overdue_id' => 'Administrative overview',
			'limitBooks' => 'Number of books',
			'limitDays' => 'Number of days',
			'limitExtensions' => 'Number of extensions',
			'creationDate' => 'Registration date',
			'deletionDate' => 'Termination date',
			'notes' => 'Notes',
			'information' => 'User Information'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('asynctoId',$this->asynctoId,true);
		$criteria->compare('aurovillename',$this->aurovillename,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('community_id',$this->community_id);
		$criteria->compare('limitBooks',$this->limitBooks);
		$criteria->compare('limitDays',$this->limitDays);
		$criteria->compare('limitExtensions',$this->limitExtensions);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size'),
			),
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterFind()
	{
        $this->creationDate = substr($this->creationDate, 0, 10);
        $this->deletionDate = substr($this->deletionDate, 0, 10);
		return parent::afterFind();
	}


	protected function beforeSave()
	{
		return parent::beforeSave();
	}

	public function showBadges()
	{
// 		echo "here be badges";
		return $this->statusBadge." ".$this->overdueBadge;
	}

	public function getStatus()
	{
		return Lookup::item('person_status', $this->status_id);
	}

    public function getCategory()
    {
        return Lookup::item('person_category', $this->category_id);
    }

	public function getStatusBadge()
	{
		$label = $this->status;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('person_status_'.$label)));
 		return $this->makeBadge($type,$label);
	}

/*
	private $_overdue_id = -1;
	public function getMaxOverdue_id()
	{
		if($this->_overdue_id != -1){
			return $this->_overdue_id;
		}
		if($this->currentcheckout)
			foreach($this->currentcheckout as $cb)
			{
				$this->_overdue_id = $this->_overdue_id > $cb->overdue_id ? $this->_overdue_id : $cb->overdue_id
			}
		return $this->_overdue_id;
	}
*/

	public function getOverdue()
	{
		return Lookup::item('overdue', $this->overdue_id);
	}

	public function getOverdueBadge()
	{
		$label = $this->overdue;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('overdue_'.$label)));
 		return $this->makeBadge($type,$label);
	}

	public function getCanCheckout()
	{
		return $this->id && $this->overdue_id != Person::OVERDUE_DISABLED;
	}

	public function getCommunity()
	{
		return Lookup::item('community', $this->community_id);
	}

	public function getPresence()
	{
		return Lookup::item('person_presence', $this->presence_id);
	}

	public function getSummary()
	{
		if($this->aurovillename)
// 			return $this->aurovillename.", ".$this->community.($this->masterlistId ? " [".$this->masterlistId."]" : "").$this->status." (".$this->id.")";
// 			return $this->aurovillename." - ".$this->name." ".$this->surname.", ".$this->community." (".$this->id.")";
			return $this->aurovillename." - ".$this->name." ".$this->surname.", ".$this->community;
        elseif($this->name)
// 			return "- ".$this->name." ".$this->surname.", ".$this->community." (".$this->id.")";
			return "- ".$this->name." ".$this->surname.", ".$this->community;
	}

	public function getSummaryRich()
	{
		if($this->aurovillename)
			return $this->aurovillename.", ".$this->community.($this->masterlistId ? " [".$this->masterlistId."]" : "").$this->showBadges();
	}

	public function getContactPhone()
	{
		if(count($this->contactdetails))
		{
			$p = array();
			foreach($this->contactdetails as $c)
			{
				if($c->contactType_id == Contactdetail::TYPE_TELEPHONE)
					$p[] = $c->contact;
				if(count($p))
					return implode(",", $p);
			}
		}
	}

	public function getContactEmail()
	{
		if(count($this->contactdetails))
		{
			$p = array();
			foreach($this->contactdetails as $c)
			{
				if($c->contactType_id == Contactdetail::TYPE_EMAIL)
					$p[] = $c->contact;
				if(count($p))
					return implode(",", $p);
			}
		}
	}

	public function getInformation()
	{
		$r = "";
/*
		if($this->community)
			$r .= $this->community."\n";
*/
		if($this->contactPhone)
			$r .= $this->contactPhone."\n";
		if($this->contactEmail)
			$r .= $this->contactEmail."\n";
		if($this->limitBooks && $this->limitDays)
			$r .= "Can borrow ".$this->limitBooks." for ".$this->limitDays." days\n";
		if($this->creationDate)
			$r .= "Registered since ".$this->creationDate;

		return $r;
	}

	public function getQuota()
	{
		$intQuota = $this->limitBooks - count($this->currentcheckout);
/*
		if($intQuota > 0)
			return $intQuota;
		else
			return 0;
*/      return $intQuota;
	}

	public function getQuotaMessage()
	{
		if($this->quota > 1)
			return "Can borrow ".$this->quota." more books";
		if($this->quota == 1)
			return "Last book !";
        if($this->quota == 0)
            return "Reached quota. Should not borrow any more books !";
        if($this->quota == -1)
            return "1 book over quota. Should not borrow any more books !";
        else
            return (-1*$this->quota)." books over quota. Should not borrow any more books !";
	}

	public function getQuotaMessageRich()
	{
/*
		if($this->quota > 1)
			$class = 'alert alert-success';
		if($this->quota == 1)
			$class = 'alert alert-warning';
		if($this->quota == 0)
			$class = 'alert alert-important';

		return "<div class='".$class."'>".$this->quotaMessage."</div>";
*/

		if($this->quota > 1)
			$type = 'success';
		if($this->quota == 1)
			$type = 'warning';
		if($this->quota <= 0)
			$type = 'important';

		return Yii::app()->controller->widget(
	    'bootstrap.widgets.TbLabel',
	    array(
	        'type' => $type,
	        // 'success', 'warning', 'important', 'info' or 'inverse'
	        'label' => $this->quotaMessage,
	    ),
	    true
		);
	}

	private static $_items = array();
	public static function items()
	{
		if(empty(self::$_items))
			self::loadItems();
		return self::$_items;
	}

	public static function item($id)
	{
		return self::model()->findByPk($id);
	}

	private static function loadItems()
	{
		self::$_items=array();
		$models=self::model()->findAll(array(
			'order'=>'aurovillename',
		));
		foreach($models as $model)
			self::$_items[$model->id]=$model->getSummary();
	}

	public function getHasEmail()
	{
  	if($this->emails && count($this->emails) && $this->emails[0] != "" && $this->emails[0] != "--")
  	  return true;
    else
      return false;
	}
}
?>