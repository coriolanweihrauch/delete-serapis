<?php
abstract class MeterDownloader extends Meter{

	abstract public function downloadReading();

	abstract public function reprocessReading();

// 	abstract public function openWebportal();

}
?>