<?php
$model = new Book_Keyword();
$model->unsetAttributes();
$model->book_id = $book_id;

$gridDataProvider=$model->search();

$buttonPlus = CHtml::ajaxLink(
	CHtml::tag('i', array('class'=>'icon-plus'), ""),
	$this->createUrl('book_Keyword/ajaxCreate'),
	array(
		'type' => 'POST',
		'data' => array(
			'Book_Keyword[book_id]'      => $model->book_id,
			'Book_Keyword[keyword_id]'   => 'js:$("#keyword_id").val()',
		),
		'success' => 'function(html){ $.fn.yiiGridView.update("book_keyword-grid"); }',
		'error' => 'function(html){ console.log(html.responseText); alert(html.responseText); }',
	),
	array(
		'data-original-title' => 'Assign to book',
		'data-toggle' => "tooltip",
	)
);

$buttonCreate = CHtml::link(
    CHtml::tag('i', array('class'=>'fa fa-list'), ""),
    Yii::app()->createUrl('keyword/create'),
    array(
        'target'=>'_blank',
        'data-original-title' => 'Create',
		'data-toggle' => "tooltip",
	),
	true
);

$gridColumns = array(
		array(
			'header' => $model->getAttributeLabel('keyword_id'),
			'name' => 'keyword_id',
			'value' => 'CHtml::link($data->keyword->summary, array("keyword/update", "id"=>$data->keyword_id), array("target"=>"_blank"), true)',
			'type' => 'raw',
			'footer' => CHtml::hiddenField('keyword_id' , 'value', array('id' => 'keyword_id')).$this->widget(
				'zii.widgets.jui.CJuiAutoComplete',
				array(
			    'name'=>'book_keyword_name',
				'sourceUrl'=>$this->createUrl('keyword/ajaxItem'),
			    'options'=>array(
			        'minLength'=>'2',
			        'select'=>"js: function(event, ui) {
							 		$('#keyword_id').val(ui.item['id']);
								}",
			    ),
			    'htmlOptions'=>array(
						'placeholder' => "New ".$model->getAttributeLabel('keyword_id'),
						'class' => 'span-14'
					),
				),
				true),
		),
		array(
			'name' => 'notes',
			'value' => '$data->keyword->notes',
		),
		array(
			'class'=> 'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'remove',
					'options'=>array('title'=>'Permanently erase'),
					'url' => 'Yii::app()->createUrl("book_Keyword/delete", array("id"=>$data->id))',
				),
				'update' => array(
// 					'icon' => 'eye-open',
					'options' => array('title'=>'Update'),
					'url' => 'Yii::app()->createUrl("keyword/update", array("id"=>$data->keyword_id))',
				),
			),
			'footer' => $buttonPlus." ".$buttonCreate,
		),
);

$this->widget(
  'bootstrap.widgets.TbGridView',
  array(
      	'id'=>'book_keyword-grid',
		'type' => Lookup::item('user_settings', 'ui_gridview_type'),
        'dataProvider' => $gridDataProvider,
		'template'=>"{items}\n{pager}",
        'columns' => $gridColumns,
		'pager' => array(
		  'class' => 'bootstrap.widgets.TbPager',
		  'displayFirstAndLast' => true,
		),
		'afterAjaxUpdate'=>"js:cgridReloadHandlerKeywordBook", // Crucial: no () after function. Calls on Load !!
  )
);


Yii::app()->clientScript->registerScript('cgridReloadHandlerKeywordBookScript', '
function cgridReloadHandlerKeywordBook(id,data){
    jQuery("#book_keyword_name").autocomplete({"minLength":"2","select": function(event, ui) { $("#keyword_id").val(ui.item["id"]); },"source":"/keyword/ajaxItem"});
}');