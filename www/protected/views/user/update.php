<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Manage Staff'=>array('admin'),
	'Update',
);

/*
$this->menu=array(
	array('label'=>'Manage users', 'url'=>array('admin')),
	array('label'=>'Create user', 'url'=>array('create')),
);
*/
?>

<h1>Update <?php echo $model->email; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>