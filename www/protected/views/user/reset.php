<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Activate';
$this->breadcrumbs=array(
	'Reset code',
);
?>

<div class="sbox reset">
<h1>Check your email</h1>

We sent a link to <?php echo $model->email; ?>. Please check your email and click that link.

<p><br/></p>

<?php
$this->widget(
  'bootstrap.widgets.TbButton',
  array(
		'type' => 'info',
		'label' => 'Re-send email',
		'url' => array('user/reset', 'id'=>$model->id, 'resend'=>true)
  )
);
?>

</div>