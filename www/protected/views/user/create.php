<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Manage Staff'=>array('admin'),
	'Create',
);
?>

<h1>Create Staff</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>