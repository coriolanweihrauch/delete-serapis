<?php
$gridDataProvider = new CArrayDataProvider($books);
// $gridDataProvider->pagination = array('pageSize'=>Lookup::Item('user_settings','ui_gridview_size'));
$gridDataProvider->pagination = $_GET['showall'] == 1 ? false : array('pageSize'=>Lookup::Item('user_settings','ui_gridview_size'));
$gridDataProvider->sort = array('attributes' => array('sn','title','languages','ddc','number','status_id'));

$model = new Checkout();
$book = new Book();

if(! $author)
$author = new Author;

// $gridColumns
$gridColumns = array(
	'sn'=> array(
		'name' => 'sn',
		'value' => '$data->barcode',
		'header'=>$book->getAttributeLabel('sn'),
	),
	'title'=>	array(
		'name' => 'title',
		'value' => Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" ?
		                'CHtml::link($data->title, array("book/update","id"=>$data->id)).$data->extraTitlesRich' :
		                'CHtml::link($data->title, array("book/view","id"=>$data->id)).$data->extraTitlesRich',
		'type' => 'raw',
		'header'=>$book->getAttributeLabel('title'),
	),
	'authorRole' => array(
    	'name' => 'authorRole',
		'header'=>$book->getAttributeLabel('authorRole'),
		'value' => '$data->authorRole('.$author->id.')'

	),
	'author' => array(
		'name' => 'author',
		'type' => 'raw',
		'value' => '$data->authorNamesLinks',
		'header'=>$book->getAttributeLabel('authorName'),
	),
	'languages'=> array(
		'name' => 'languages',
		'value' => '$data->languages',
		'header'=>$book->getAttributeLabel('languages'),
	),
    "ddc" => array(
    	'name' => 'ddc',
		'header'=>$book->getAttributeLabel('ddc'),
    ),
	"number" => array(
		'name' => 'number',
		'header'=>$book->getAttributeLabel('number'),
	),
	'status_id'=>array(
		'name' => 'status_id',
		'type'=>'raw',
		'value' => '$data->statusBadge',
		'header'=>$book->getAttributeLabel('status_id'),
	),
/*
	array(
		'name' => 'borrowDate',
		'value' => '$data->borrowDate',
		'header'=>$book->getAttributeLabel('borrowDate'),
	),
	array(
		'name' => 'dueDate',
		'value' => '$data->dueDate',
		'header'=>$book->getAttributeLabel('dueDate'),
	),
	array(
		'name' => 'returnDate',
		'value' => '$data->returnDate',
		'header'=>$book->getAttributeLabel('returnDate'),
	),
*/
);

if(is_array($arrHide))
	foreach($arrHide as $rem)
	{
		unset($gridColumns[$rem]);
	}

if($author->isNewRecord)
    unset($gridColumns['author_role']);


$this->widget(
    'bootstrap.widgets.TbGridView',
    array(
   		'type' => Lookup::item('user_settings', 'ui_gridview_type'),
        'dataProvider' => $gridDataProvider,
        'template' => "{items}",
// 				'filter' => $model->search(),
        'columns' => $gridColumns,
    	'pager' => array(
    	  'class' => 'bootstrap.widgets.TbPager',
    	  'displayFirstAndLast' => true,
    	),
        'template'=>"{summary}\n{items}\n{pager}",
    )
);

if( $_GET['showall'] != 1)
    echo CHtml::link( "Show all", array_merge(array(Yii::app()->request->getPathInfo()), $_GET, array('showall'=>true)));
?>