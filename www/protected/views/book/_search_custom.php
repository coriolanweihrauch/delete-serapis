<div class="well">Customise the search filters in the table below<br/>(This will not perform any search, but give you the option to filter on any combination of fields)</div>
<?php
$arrAttr = array();
foreach($model->advancedSearchCustomAttributes as $id => $attr)
  $arrAttr[$attr] = $model->getAttributeLabel($attr);

echo $form->checkBoxListRow(
  $model,
  'search_advanced_custom_fields',
  $arrAttr,
  array()
);