<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Books'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Report', 'url'=>array('admin')),
);
?>

<h1>Create Book</h1>

<?php $this->renderPartial('_form_create', array('model'=>$model)); ?>