<?php
/* @var $this personController */
/* @var $model person */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Report',
);

$this->menu=array(
	// Export to CSV instructions
	// http://www.yiiframework.com/extension/exportablegridbehavior
	// 1. copy 'behaviours' function of controller
	// 2. add 'if($this->isExportRequest())' block to actionAdmin function of controller
	// 3. add link below and put correct gridview id in BOTH parameters
// 	array('label'=>'Export as CSV', 'url'=>"javascript:window.location=$('#person-grid').yiiGridView('getUrl')+(($('#person-grid').yiiGridView('getUrl').indexOf('?')==-1)?'?':'&')+'exportCSV=1'"),
	array('label'=>'Create User', 'url'=>array('create')),
);
?>

<h1>Users</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'person-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
	'selectableRows'=>1,
	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'/id/"+$.fn.yiiGridView.getSelection(id);}',
	'columns'=>array(

// Aurovillename	Name	Surname	Workplace	Address	Telephone	Number of Books	Number of days	New ID


		array(
			'name'=>'aurovillename',
			'type'=>'raw',
			'value' => 'CHtml::link($data->aurovillename, array("person/view","id"=>$data->id))',
		),
		array(
			'name'=>'name',
			'type'=>'raw',
			'value' => 'CHtml::link($data->name, array("person/view","id"=>$data->id))',
		),
		array(
			'name'=>'surname',
			'type'=>'raw',
			'value' => 'CHtml::link($data->surname, array("person/view","id"=>$data->id))',
		),
		'id',
// 		'workplace',
// 		'asynctoId',
		array(
			'name' => 'category_id',
			'value' => '$data->category',
			'filter'=> CHtml::listData(Lookup::lookupList('person_category'), 'id', 'title'),
		),
		array(
			'name' => 'community_id',
			'value' => 'Lookup::item("community", "$data->community_id")',
			'filter'=> CHtml::listData(Lookup::lookupList('community'), 'id', 'title'),
		),
		'limitBooks',
		'limitDays',
		'limitExtensions',
		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {erase}',

			'buttons' => array(
				'erase' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin")',
					'label' => '<span class="icon-remove"></span>',
					'options'=>array('title'=>'Permanently erase'),
					'url' => 'Yii::app()->createUrl($data->tableName()."/erase", array("id"=>$data->id))',
					'click' => <<<EOD
						function() {
							if(!confirm('Permanently erase ?')) return false;
							$.fn.yiiGridView.update('person-grid', {
								type:'POST',
								url:$(this).attr('href'),
								success:function(text,status) {
									if(text && text.error)
									{
										console.log('error');
										console.log(text);
										alert("Error: "+text.error);
									}
									else
									{
										$.fn.yiiGridView.update('person-grid');
									}
								},
								failure:function(text,status){
									alert('failure');
								},
								error:function(text,status){
									alert('error');
								},
								dataType: 'json',
							});
							return false;
						}
EOD
				),
			),
		),
	),
));
?>