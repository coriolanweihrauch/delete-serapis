<?php
/* @var $this PersonController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->summary=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'View', 'url'=>array('view','id'=>$model->id)),
// 	array('label'=>'Report', 'url'=>array('index')),
);
?>

<h1>Update '<?php echo $model->summary; ?>'</h1>
<div class="badgelist"><?php
echo $model->showBadges();
?></div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<h2>Contact Details</h2>
<div id="Contactdetail">
<?php $this->renderPartial('//contactdetail/_list', array('models'=>$model->contactdetails, 'edit'=>true)); ?>
</div>

<h2>Lending History</h2>
<div id="Checkout">
<?php $this->renderPartial('//checkout/_list', array('person_id'=>$model->id, 'arrHide'=>array('person'))); ?>
</div>

<?php
// Pop-up edit box for contactdetails
//	http://www.yiiframework.com/wiki/263/cgridview-update-create-records-in-a-cjuidialog/
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogContactdetail',
    'options'=>array(
        'title'=>'Add Contact Detail',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>550,
        'height'=>300,
    ),
));?>
<div class="divForForm"></div>
<?php $this->endWidget();?>

<script type="text/javascript">
function addContactdetail()
{
	person_id = <?php echo $model->id; ?>;
<?php echo CHtml::ajax(array(
        'url'=>array('Contactdetail/ajaxEdit'),
				'data'=>"js:person_id ? {person_id:person_id} : $(this).serialize()",
        'type'=>'post',
        'dataType'=>'json',
        'success'=>"function(data)
        {
	        if (data.status == 'failure')
	        {
            $('#dialogContactdetail div.divForForm').html(data.div);
            $('#dialogContactdetail div.divForForm form').submit(addContactdetail);
	        }
	        else
	        {
            $('#dialogContactdetail div.divForForm').html(data.div);
            setTimeout(\"$('#dialogContactdetail').dialog('close') \",500);
	        }
        }",
        ))?>;
  return false;
}

function editContactdetail(contactdetail_id)
{
	$('#dialogContactdetail').dialog('open');
	$('#dialogContactdetail span.ui-dialog-title').text('edit');
<?php echo CHtml::ajax(array(
        'url'=>array('contactdetail/ajaxEdit'),
				'data'=>'js:{id:contactdetail_id}',
        'type'=>'post',
        'dataType'=>'json',
        'success'=>"function(data)
        {
          if (data.status == 'failure')
          {
          	console.log('failure');
            $('#dialogContactdetail div.divForForm').html(data.div);
            $('#dialogContactdetail div.divForForm form').submit(editContactdetail);
          }
          else
          {
           	console.log('success');
            $('#dialogContactdetail div.divForForm').html(data.div);
            setTimeout(\"$('#dialogContactdetail').dialog('close') \",3000);
          }
        } ",
        ))?>;
	$('span.ui-dialog-title').text('Update Contactdetail');
}

function closeContactdetail()
{
	$('#dialogContactdetail').dialog('close');
<?php echo CHtml::ajax(array(
        'url'=>array('contactdetail/ajaxList', 'person_id'=>$model->id),
        'type'=>'get',
        'dataType'=>'json',
        'success'=>"function(data)
        {
          if(data.status == 'failure')
          {
          	console.log('error getting contactdetail');
          }
          else
          {
						$('#Contactdetail').html(data.div);
						console.log('updated contactdetail');
          }
        }",
        ))?>;
  return false;
}

function deleteContactdetail(e)
{
	contactdetail_id = $(e).parent().parent().attr('id');

	bootbox.confirm("Are you sure you want to delete this contact detail entry ?",
	function(confirmed){
    if(confirmed) {

<?php echo CHtml::ajax(array(
        'url'=>array('contactdetail/ajaxDelete'),
				'data'=>'js:{id:contactdetail_id}',
        'type'=>'get',
        'dataType'=>'json',
        'success'=>"function(data)
        {
          if(data.status == 'failure')
          {
          	alert('error deleting');
          }
          else
          {
          	console.log('deleted successfully');
						$('#Contactdetail').html(data.div);
          }
        }",
        ))?>;
    }
	});
}
</script>