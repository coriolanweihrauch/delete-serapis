<?php
/* @var $this meterController */
/* @var $model f */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'feature-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'name'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

<?php
  echo $form->textFieldRow(
    $model,
    'name',
    array(
        'class' => 'span8',
    )
  );
?>
    </fieldset>
    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
    </div>
<?php $this->endWidget(); ?>