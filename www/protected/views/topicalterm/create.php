<?php
$this->breadcrumbs=array(
	'Topical Term'=>array('admin'),
	'Create',
);
?>

<h1>Create Topical Term</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>