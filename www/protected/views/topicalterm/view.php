<?php
$this->breadcrumbs=array(
	'Topical Term'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Update', 'url'=>array('update','id'=>$model->id)),
);
?>
<h1><?php echo $model->name; ?> <small><?php echo $model->alternateTerm; ?></small></h1>
<?php print($model->notes); ?>

<h2>Linked books</h2>
<div id="checkout">
<?php $this->renderPartial('//book/_list', array('books'=>$model->books, 'arrHide'=>array('authorRole'))); ?>
</div>