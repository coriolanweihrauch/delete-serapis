<?php
/* @var $this LookupController */
/* @var $model Lookup */

$this->breadcrumbs=array(
	'Manage Lookups'=>array('admin'),
	'Create',
);
?>

<h1>Create new lookup</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>