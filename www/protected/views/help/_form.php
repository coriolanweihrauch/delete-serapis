<?php
/* @var $this meterController */
/* @var $model f */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'help-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
)); ?>

	<?php echo $form->errorSummary($model); ?>
    <fieldset>
	<div id="metadata_cover">
	</div>

<?php

	echo$form->ckEditorRow(
    $model,
    'content',
    array(
      'editorOptions' => array(
        'fullpage' => 'js:true',
        'width' => '1000',
        'height' => '500',
        'toolbar' => 'MyToolbar',
/*         'toolbar' => 'Basic', */
        'toolbar_MyToolbar' => array(
        	array(
        		'name'=>'history','items'=>array('Undo','Redo'),
        	),
        	array(
        		'name'=>'clipboard','items'=>array('Cut','Copy','Paste','-','PasteText','PasteFromWord',),
        	),
        	array(
        		'name'=>'document','items'=>array('Print','Preview','Source','Maximize'),
        	),
        	array(
        		'name'=>'special','items'=>array('Table','SpecialChar','PageBreak','Styles','Format'),
        	),
					'/',
					array(
						'name'=>'formatting', 'items'=>array('Bold','Italic','Underline','Subscript','Superscript','-','RemoveFormat'),
					),
					array(
						'name'=>'font', 'items'=>array('Font','FontSize'),
					),
					array(
						'name'=>'colors', 'items'=>array('TextColor','BGColor'),
					),
					array(
						'name'=>'paragraph', 'items'=>array('JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),
					),
					array(
						'name'=>'dent', 'items'=>array('Outdent','Indent'),
					),
					array(
						'name'=>'list', 'items'=>array('NumberedList','BulletedList'),
					),
        ),
/*
config.toolbar_Full =
[
	{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	'/',
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
];
*/
      )
    )
  );?>
    </fieldset>
    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
				<?php
				$url = Yii::app()->createAbsoluteUrl('help/admin');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
    </div>
<?php $this->endWidget(); ?>