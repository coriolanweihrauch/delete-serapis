<h1>Merge</h1>
<?php
/* @var $this meterController */
/* @var $model f */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'book-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'sn'),
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php
echo CHtml::hiddenField('activeTab' , $activeTab, array('id' => 'activeTab'));

$this->widget(
  'bootstrap.widgets.TbTabs',
  array(
    'type' => 'tabs', // 'tabs' or 'pills'
		'id'=>'mergetabs',
    'tabs' => array(
      array(
        'label' => 'Authors',
        'id' => 'tab_author',
        'content' => $this->renderPartial('_merge_author', array('model'=>$model, 'form'=>$form), true),
				'icon' => 'fa fa-pencil fa-fw',
				'active' => $activeTab == 'tab_author' || ! $activeTab ? true : false,
      ),
      array(
      	'label' => 'Keywords',
        'id' => 'tab_keyword',
        'content' => $this->renderPartial('_merge_keyword', array('model'=>$model, 'form'=>$form), true),
				'icon' => 'fa fa-key fa-fw',
				'active' => $activeTab == 'tab_keyword' ? true : false,
      ),
      array(
      	'label' => 'Topical Term',
        'id' => 'tab_topicalterm',
        'content' => $this->renderPartial('_merge_topicalterm', array('model'=>$model, 'form'=>$form), true),
				'icon' => 'fa fa-th fa-fw',
				'active' => $activeTab == 'tab_topicalterm' ? true : false,
      ),
    ),
  )
);

?>
<div class="form-actions">
<?php $this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => 'Merge',
  	'htmlOptions' => array(
			'onclick' => "$('#bExport').val(0);$('#activeTab').val( $('#mergetabs ul li.active a').attr('href').replace(/^.*#/, '') );",
		),
  )
); ?>
</div>
<?php $this->endWidget(); ?>