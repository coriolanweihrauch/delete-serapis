<?php
$book = new Book();
$model = new Checkout();
if($book_id)
	$model->book_id = $book_id;
if($person_id)
	$model->person_id = $person_id;
if($returnDate)
	$model->returnDate = $returnDate;

$gridDataProvider = $model->searchList();

// $gridColumns
$gridColumns = array(

	'sn'=> array(
		'name' => 'sn',
		'value' => '$data->book->barcode',
		'header'=>$book->getAttributeLabel('sn'),
	),
	'title'=>	array(
		'name' => 'title',
		'value' => Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" ?
		                'CHtml::link($data->book->title, array("book/update","id"=>$data->book->id)).$data->book->extraTitlesRich' :
		                'CHtml::link($data->book->title, array("book/view","id"=>$data->book->id)).$data->book->extraTitlesRich',
		'type' => 'raw',
		'header'=>$book->getAttributeLabel('title'),
	),
	'author' => array(
		'name' => 'author',
		'type' => 'raw',
		'value' => '$data->book->authorNamesLinks',
		'header'=>$book->getAttributeLabel('authorName'),
	),
	'languages'=> array(
		'name' => 'languages',
		'value' => '$data->book->languages',
		'header'=>$book->getAttributeLabel('languages'),
	),
	'ddc'=> array(
		'name' => 'ddc',
		'value' => '$data->book->ddc',
		'header'=>$book->getAttributeLabel('ddc'),
	),
	'number'=> array(
		'name' => 'number',
		'value' => '$data->book->number',
		'header'=>$book->getAttributeLabel('number'),
	),
	'person'=>	array(
		'name' => 'person',
		'value' => 'CHtml::link($data->person->summary, array("person/view","id"=>$data->person_id))',
		'type' => 'raw',
		'header'=> 'User',
	),
	'borrowDate' => array(
		'name' => 'borrowDate',
		'value' => '$data->borrowDate',
		'header'=>$book->getAttributeLabel('borrowDate'),
	),
	'dueDate' => array(
		'name' => 'dueDate',
		'value' => '$data->dueDate',
		'header'=>$book->getAttributeLabel('dueDate'),
	),
	'returnDate' => array(
		'name' => 'returnDate',
		'value' => '$data->returnDate',
		'header'=>$book->getAttributeLabel('returnDate'),
	),
	'status' => array(
		'name' => 'status',
		'type' => 'raw',
		'value' => '$data->statusBadge',
		'header'=>$book->getAttributeLabel('status'),
	),
	'extend' => array(
		'class'=> 'bootstrap.widgets.TbButtonColumn',
		'header' => 'Extend',
		'template' => '{extend}',
		'buttons' => array(
			'extend' => array(
				'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" || Yii::app()->user->roles == "staff") && $data->status_id == 2 || $data->status_id == 3 || $data->status_id == 4 ',
				'icon' => 'fa fa-arrows-h',
				'options'=>array('title'=>'Extend'),
				'url' => 'Yii::app()->createUrl("checkout/extend", array("id"=>$data->id))',
			),
		),
	),
);

if(is_array($arrHide))
	foreach($arrHide as $rem)
	{
		unset($gridColumns[$rem]);
	}

$this->widget(
    'bootstrap.widgets.TbGridView',
    array(
	    	'id'=>'checkout-grid',
    		'type' => Lookup::item('user_settings', 'ui_gridview_type'),
        'dataProvider' => $gridDataProvider,
				'template'=> Lookup::item('user_settings', 'ui_gridview_template'),
// 				'filter' => $model->search(),
        'columns' => $gridColumns,
				'pager' => array(
				  'class' => 'bootstrap.widgets.TbPager',
				  'displayFirstAndLast' => true,
				),
    )
);

if( $_GET['showall'] != 1)
    echo CHtml::link( "Show all", array_merge(array(Yii::app()->request->getPathInfo()), $_GET, array('showall'=>true)));
?>