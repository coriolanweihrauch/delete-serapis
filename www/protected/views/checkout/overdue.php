<?php
$this->breadcrumbs=array(
	'List'=>array('admin'),
	'Overdue',
);
?>

<h1>Overdues</h1>


<?php
$countWithoutEmail = $model->overduesWithoutEmail();
if($countWithoutEmail)
{
  Yii::app()->controller->widget(
    'bootstrap.widgets.TbLabel',
    array(
  	  'type' => 'important',
  	  'label' => $countWithoutEmail > 1 ? $countWithoutEmail." users without email" : "1 user without email",
    )
  );
}

$countReadyToSend = $model->overduesReadyToSend();
$countText = $countReadyToSend > 1 ? $countReadyToSend." reminders" : $countReadyToSend." reminder";
if($countReadyToSend)
{
  Yii::app()->controller->widget(
    'bootstrap.widgets.TbLabel',
    array(
  	  'type' => 'info',
  	  'label' => "Ready to send ".$countText,
    )
  );

  echo "<br/><br/>";
  $this->widget(
    'bootstrap.widgets.TbButton',
    array(
      'label' => 'Send '.$countText,
      'htmlOptions' => array(
        'onclick' => 'bootbox.confirm("Do you really want to send '.$countText.' ?",
          function(confirmed){
            if(confirmed) {
              window.location = "'. Yii::app()->createAbsoluteUrl('checkout/enqueue').'";
            }
          })',
      ),
    )
  );
}


$book=new Book();

	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'Overdue-grid',
	'dataProvider'=>$model->search($_GET['showall']),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
  'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),

	'columns'=>array(
/*
		array(
			'name' => 'status_id',
			'type' => 'raw',
			'value' => '$data->statusBadge',
			'filter' => CHtml::listData(Lookup::lookupList('checkout_status'), 'id', 'title'),
		),
*/
		'book'=>array(
			'name' => 'search_book_sn',
			'type' => 'raw',
			'value' => '$data->book->summary',
		),
		'authors' => array(
			'header' => $book->getAttributeLabel('authorName'),
			'type' => 'raw',
			'value' => '$data->book->authorNamesLinks',
		),
/*
		'languages'=> array(
			'header'=>$book->getAttributeLabel('languages'),
			'value' => '$data->book->languages',
		),
*/
		'person'=>	array(
			'name'=> 'person_id',
			'type' => 'raw',
			'value' => 'CHtml::link($data->person->summary, array("person/view","id"=>$data->person_id))',
			'filter' => false,
		),
		'borrowDate',
		'dueDate',
// 		'returnDate',
		'reminderDate',
		'reminderCount',
		'reminderStatus' => array(
		  'name' => 'reminderStatus_id',
		  'filter' => CHtml::listData(Lookup::lookupList('reminder_status'), 'id', 'title'),
		  'type' => 'raw',
		  'value' => '$data->reminderStatusBadge',
		),
	),
));

if( $_GET['showall'] != 1)
    echo CHtml::link( "Show all", array_merge(array(Yii::app()->request->getPathInfo()), $_GET, array('showall'=>true)));
?>