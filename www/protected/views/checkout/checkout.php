<h1>Check-out</h1>
<div id="badgelist" class="badgelist"><?php
if($person->id)
	echo $person->showBadges();
?></div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'checkout-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'method' => 'get',
	'focus' => $person->canCheckout ? array($book,'sn') : array($person, 'name'),
)); ?>
<fieldset>
<?php echo $form->errorSummary($person); ?>
<?php // echo $form->errorSummary($book); ?>

<div class="control-group">
	<label class="control-label" for="<?php echo "person_id"; ?>">User Name</label>
	<div class="controls">
		<div class="input-append" onClick="$('#person_name').focus();">
<?php
	echo $form->hiddenField(
    $person,
    'id'
  );

	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Person[name]',
		'sourceUrl'=>$this->createUrl('person/ajaxItem'),
		'value'=>$person->summary,

    'options'=>array(
	    'minLength'=>'2',
	    'select'=>"js: function(event, ui) {
		 		$('#Person_id').val(ui.item['id']);
		 		$('#yw0').text('Select client');
		 		$('#Book_sn').parent().parent().hide();
		 		$('#Checkout').hide();
		 		$('#badgelist').hide();
		 		$('#yw0').prop('disabled', true);
		 		$('#checkout-form').submit();
			}",
    ),
    'htmlOptions'=>array(
        'class' => 'span4',
    )
	));
?>
		<span class="add-on"><i class="fa fa-ellipsis-h"></i></span>
<?php
if($person->id){
// 	echo "can borrow ".$person->borrowingQuota."book(s)";
	echo $person->quotaMessageRich;
}
?>
		</div>
	</div>
</div>

<?php
if($person->id)
{

	echo $form->textAreaRow(
	$person,
	'information',
    array(
	    'disabled' => 'disabled',
	    'class' => 'span4',
	    'rows' => '4',
    )
	);

	echo $form->textAreaRow(
    $person,
    'notes',
    array(
	    'rows' => '5',
	    'class' => 'span4',
// 	    'disabled' => 'disabled',
    )
  );

	if($person->overdue_id == Person::OVERDUE_DISABLED)
	{
		echo '<div class="control-group"><div class="controls">';
		echo Yii::app()->controller->widget(
	    'bootstrap.widgets.TbLabel',
	    array(
	        'type' => 'warning',
	        'label' => 'User has been disabled and cannot borrow any books'
	    ),
	    true
		);
		echo "</div></div>";
	}
	else
	{
		if($book->status_id == Book::STATUS_OUT)
			$book->sn = "";

		echo $form->textFieldRow(
	    $book,
	    'sn',
        array(
    	    'rows' => 5,
    	    'class' => 'span4',
        )
	  );
	}
}
?>

</fieldset>

<div class="form-actions">
<?php
if($person->canCheckout)
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $person->canCheckout ? "Check-out" : "Select user",
    )
);

if( ! $person->isnewrecord)
{
    $url = Yii::app()->createAbsoluteUrl('person/update', array('id'=>$person->id));
    print(" ");
    $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'type' => 'info',
            'label' => "Edit user",
        	'htmlOptions' => array(
				'onclick' => 'if($("#Book_sn").val() != "") bootbox.confirm("Edit user WITHOUT checking out book ?",
												function(confirmed){
					                if(confirmed) {
					                   window.location = "'.$url.'";
					                }
							}); else window.location = "'.$url.'"',
			),
        )
    );
?>

<?php
$url2 = Yii::app()->createAbsoluteUrl('checkout/checkout');
$this->widget(
'bootstrap.widgets.TbButton',
array(
    'buttonType' => 'button',
    'label' => 'Clear',
    'type' => 'inverse',
	'htmlOptions' => array(
		'onclick' => 'window.location = "'.$url2.'"',
	),
)
);
}
?>
</div>

<?php $this->endWidget(); ?>

<?php
if($person->id)
{
?>
<h2>Currently borrowed <small><?php echo CHtml::link('View entire history', array('/person/view', 'id'=>$person->id)); ?></h2>
<div id="checkout">
<?php
	$this->renderPartial('//checkout/_list', array('person_id'=>$person->id, 'returnDate'=>'0000-00-00', 'arrHide'=>array('person')));
}
?>
</div>