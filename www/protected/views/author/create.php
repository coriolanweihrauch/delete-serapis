<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Authors'=>array('admin'),
	'Create',
);

$this->menu=array(
// 	array('label'=>'Report', 'url'=>array('admin')),
);
?>

<h1>Create Author</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>