<?php
$model = new Author_Book();
$model->unsetAttributes();
$model->book_id = $book_id;

$gridDataProvider=$model->search();

$buttonPlus = CHtml::ajaxLink(
	CHtml::tag('i', array('class'=>'icon-plus'), ""),
	$this->createUrl('author_Book/ajaxCreate'),
	array(
		'type' => 'POST',
		'data' => array(
			'Author_Book[book_id]'    		  => $model->book_id,
			'Author_Book[author_id]'          => 'js:$("#author_id").val()',
			'Author_Book[role_id]'            => 'js:$("#author_role_id").val()',
		),
		'success' => 'function(html){ $.fn.yiiGridView.update("author_Book-grid"); }',
		'error' => 'function(html){ console.log(html.responseText); alert(html.responseText); }',
	),
	array(
		'data-original-title' => 'Assign to book',
		'data-toggle' => "tooltip",
	)
);

$buttonCreate = CHtml::link(
    CHtml::tag('i', array('class'=>'fa fa-list'), ""),
    Yii::app()->createUrl('author/create'),
    array(
        'target'=>'_blank',
        'data-original-title' => 'Create',
		'data-toggle' => "tooltip",
	),
	true
);

$gridColumns = array(
		array(
			'header' => $model->getAttributeLabel('author_id'),
			'name' => 'author_id',
			'value' => 'CHtml::link($data->author->summary, array("author/view", "id"=>$data->author_id), array("target"=>"_blank"), true)',
			'type' => 'raw',
			'footer' => CHtml::hiddenField('author_id' , 'value', array('id' => 'author_id')).$this->widget(
				'zii.widgets.jui.CJuiAutoComplete',
				array(
			    'name'=>'author_name',
				'sourceUrl'=>$this->createUrl('author/ajaxItem'),
                'options'=>array(
		            'minLength'=>'2',
                    'select'=>"js: function(event, ui) {
						 		$('#author_id').val(ui.item['id']);
							}",
			    ),
			    'htmlOptions'=>array(
						'placeholder' => "New ".$model->getAttributeLabel('author_id'),
						'class' => 'span-10'
					),
				),
				true),
		),
		array(
 			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'role_id',
			'value' => '$data->roleBadge',
			'type'=>'raw',
			'filter' => CHtml::listData(Lookup::lookupList('author_role'), 'id', 'title'),
      'editable' => array(
	      'type' => 'select',
	      'source' => Lookup::items('author_role'),
        'url' => $this->createUrl('author_Book/ajaxUpdate'),
        'placement' => 'right',
        'inputclass' => 'span4',
        'attribute' => 'type_id',
				'onSave' => 'js:function(event, params) {	$("#author_Book-grid").yiiGridView("update"); }',
      ),
			'footer' => CHtml::dropDownList('role_id', null, Lookup::items('author_role'), array('empty' => '--Select--','class'=>'span1','id'=>'author_role_id')),
		),
		array(
			'name' => 'lifetime',
			'value' => '$data->author->lifetime',
		),
		array(
			'name' => 'notes',
			'value' => '$data->author->notes',
		),
		array(
			'class'=> 'bootstrap.widgets.TbButtonColumn',
// 			'template' => '{update} {view} {delete}',
			'template' => '{update} {delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'remove',
					'options'=>array('title'=>'Unlink'),
					'url' => 'Yii::app()->createUrl("author_Book/delete", array("id"=>$data->id))',
				),
				'view' => array(
					'icon' => 'eye-open',
					'options' => array('title'=>'View'),
					'url' => 'Yii::app()->createUrl("author/view", array("id"=>$data->author_id))',
				),
				'update' => array(
					'options' => array('target'=>'_blank'),
					'url' => 'Yii::app()->createUrl("author/update", array("id"=>$data->author_id))',
				),

			),
			'footer' => $buttonPlus." ".$buttonCreate,
		),
);

$this->widget(
  'bootstrap.widgets.TbGridView',
  array(
    	'id'=>'author_Book-grid',
		'type' => Lookup::item('user_settings', 'ui_gridview_type'),
        'dataProvider' => $gridDataProvider,
		'template'=>"{items}\n{pager}",
        'columns' => $gridColumns,
		'pager' => array(
		  'class' => 'bootstrap.widgets.TbPager',
		  'displayFirstAndLast' => true,
		),
		'afterAjaxUpdate'=>"js:cgridReloadHandlerAuthorBook",
  )
);


Yii::app()->clientScript->registerScript('cgridReloadHandlerAuthorBook', '
function cgridReloadHandlerAuthorBook(id,data){
    jQuery("#author_name").autocomplete({"minLength":"2","select": function(event, ui) { $("#author_id").val(ui.item["id"]); },"source":"/author/ajaxItem"});
}');