<?php
/* @var $this HousingassetController */
/* @var $model Housingasset */

$this->breadcrumbs=array(
	'Authors'=>array('index'),
	$model->summary,
	'view'
);

$this->menu=array(
	array('label'=>'Update', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'List', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->summary; ?></h1>
<div class="badgelist"><?php
echo $model->showBadges();
?></div>

<?php
 $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'name',
		'nameFullerForm',
		'lifetime',
		'notes',
	),
));
// var_dump($model);
?>
<h2>Publications</h2>
<div id="checkout">
<?php $this->renderPartial('//book/_list', array('books'=>$model->books, 'arrHide'=>array('languages'), 'author'=>$model)); ?>
</div>
